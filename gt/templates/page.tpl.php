<?php
/**
 * @file
 * GT theme's implementation to display a single Drupal page.
 *
 * See README for variables explanation.
 *
 */
?>

<div id="page">

    <header id="masthead">

        <section id="identity" role="banner">
            <div id="identity-wrapper" class="clearfix">
                <h1 id="gt-logo">
                    <?php print $gt_logo_file; ?>
                    <a href="http://www.gatech.edu" id="gt-logo-mothership-link"
                       title="Georgia Institute of Technology">Georgia Institute of Technology</a>
                       <?php if ($gt_logo_right_url != '') : ?>
                      <a href="<?php print $gt_logo_right_url; ?>" id="gt-logo-secondary-link"
                         title="<?php print $gt_logo_right_title; ?>"><?php print $gt_logo_right_title; ?></a>
                       <?php endif; ?>
                </h1>
                <?php if ($site_title != '') : ?>
                  <h2 class="<?php print $site_title_class; ?>" id="site-title" rel="home"><a
                          href="<?php print $front_page; ?>"><?php print $site_title; ?></a></h2>
                    <?php endif; ?>
            </div>
        </section><!-- /#identity -->

        <section id="primary-menus">
            <div id="primary-menus-wrapper" class="clearfix">
                <a id="primary-menus-toggle" class="hide-for-desktop"><span>Menu</span></a>
                <div id="primary-menus-off-canvas" class="off-canvas">
                    <a id="primary-menus-close" class="hide-for-desktop"><span>Close</span></a>
                    <nav>
                        <div id="main-menu-wrapper" role="navigation" aria-label="Primary">
                            <?php if ($main_menu) : ?>
                              <?php print $primary_main_menu; ?>
                              <?php
                              if ($is_admin) : print $primary_main_menu_manage;
                              endif;
                              ?>
                            <?php endif; ?>
                        </div>
                        <div id="action-items-wrapper" role="navigation" aria-label="Action">
                            <?php if ($action_items): ?>
                              <?php print theme('links', array('links' => $action_items, 'attributes' => array('id' => 'action-items'))); ?>
                              <?php
                              if ($is_admin) : print $action_items_manage;
                              endif;
                              ?>
                            <?php else : ?>
                              <?php
                              if ($is_admin) : print $action_items_add;
                              endif;
                              ?>
                            <?php endif; ?>
                        </div>
                    </nav>
                    <div id="utility">
                        <div class="row clearfix">

                            <!-- #utility-links -->
                            <?php print $utility_links_content; ?>
                            <!-- /#utility-links -->

                            <div id="social-media-links-wrapper" role="navigation" aria-label="Social Media">
                                <?php if ($social_media_links): ?>
                                  <?php print theme('links', array('links' => $social_media_links, 'attributes' => array('id' => 'social-media-links'))); ?>
                                  <?php
                                  if ($is_admin) : print $social_media_links_manage;
                                  endif;
                                  ?>
                                <?php else : ?>
                                  <?php
                                  if ($is_admin) : print $social_media_links_add;
                                  endif;
                                  ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div><!-- /#utility -->
                </div>
                <div id="site-search" class="search-local">
                    <?php if (module_exists('search')) : ?>
                      <a href="<?php print $base_path; ?>search" id="site-search-container-switch" class="element-focusable">Search</a>
                      <div id="site-search-container" class="element-invisible">
                          <?php print $search_page; ?>
                      </div>
                    <?php endif; ?>
                </div><!-- /#site-search -->
            </div><!-- /#primary-menus-wrapper -->
            <div id="breadcrumb" class="hide-for-mobile">
                <div class="row clearfix">
                    <ul><?php print $breadcrumb; ?></ul>
                </div>
            </div><!-- /#breadcrumb -->
        </section><!-- /#primary-menus -->

        <?php $spotlight = render($page['spotlight']); ?>

        <?php if ($spotlight) : ?>
          <section id="header-spotlight">
              <?php print $spotlight; ?>
          </section>
        <?php endif; ?>

    </header><!-- /#masthead -->

    <section role="main" id="main"<?php
    if ($spotlight) : print ' class="with-spotlight"';
    endif;
    ?>>
        <div class="row clearfix">

            <?php print render($title_prefix); ?>
            <?php if ($title): ?>
              <div id="page-title">
                  <h2 class="title"><?php print $title; ?></h2>
              </div>
            <?php endif; ?>
            <?php print render($title_suffix); ?>

            <?php
            // Check for content lead/close and left_nav
            $content_lead = render($page['content_lead']);
            $content_close = render($page['content_close']);
            ?>

            <?php if ($content_lead) : ?>
              <div id="content-lead">
                  <?php print $content_lead; ?>
              </div>
            <?php endif; ?>

            <div class="<?php print $content_class; ?>" id="content">

                <?php
                // Check for content page help and tabs
                $page_help = render($page['help']);
                $page_tabs = render($tabs);
                ?>

                <?php if ($messages || $page_help || $page_tabs || $action_links) : ?>
                  <div id="support">
                      <?php print $messages; ?>
                      <?php print render($page['help']); ?>
                      <?php print render($tabs); ?>
                      <?php if ($action_links) : ?>
                        <ul class="action-links">
                            <?php print render($action_links); ?>
                        </ul>
                      <?php endif; ?>
                  </div>
                <?php endif; ?>
                <?php print render($page['main_top']); ?>
                <?php print render($page['content']); ?>
                <?php print render($page['main_bottom']); ?>
                <?php print $feed_icons; ?>
            </div><!-- /#content -->

            <?php
            // Render the sidebars to see if there's anything in them.
            $left_nav = render($page['left_nav']);
            $sidebar_left = render($page['left']);
            $sidebar_right = render($page['right']);
            ?>

            <?php if ($left_nav || $sidebar_left || $sidebar_right): ?>
              <div aria-label="Sidebars" role="complementary">

                  <?php if ($left_nav || $sidebar_left): ?>
                    <aside id="sidebar-left" class="<?php print $sidebar_left_class; ?>">
                        <?php if ($left_nav) : ?>
                          <nav id="left-nav" role="navigation" aria-label="Local">
                              <?php print $left_nav; ?>
                          </nav>
                        <?php endif; ?>
                        <?php print $sidebar_left; ?>
                    </aside>
                  <?php endif; ?>

                  <?php if ($sidebar_right): ?>
                    <aside id="sidebar-right" class="<?php print $sidebar_right_class; ?>">
                        <?php print $sidebar_right; ?>
                    </aside>
                  <?php endif; ?>
              </div>
            <?php endif; ?>

            <?php if ($content_close) : ?>
              <div id="content-close">
                  <?php print $content_close; ?>
              </div>
            <?php endif; ?>

        </div>
    </section><!-- /#main -->

    <div id="contentinfo" aria-label="Footers">
        <!-- #superfooter/ -->
        <?php print $superfooter_content; ?>
        <!-- /#superfooter -->

        <!-- #footer/ -->
        <?php print $footer_content; ?>
        <!-- /#footer -->

    </div>
</div><!-- /#page -->

<?php print render($page['bottom']); ?>
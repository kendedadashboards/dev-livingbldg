About the Georgia Tech Drupal Theme
============================================================
This is the official Drupal 7.x theme for Georgia Tech 
websites, which provides the new UI that was established in 
September of 2013, plus all required branding and content 
elements. For more details about this theme visit: 
https://drupal.gatech.edu/node/156

This theme is maintained by the web team in Institute 
Communications with contributions from the Georgia Tech 
Drupal community. To learn more about the Drupal community 
and how to contribute please visit:
https://drupal.gatech.edu

For any questions, comments, or concerns with this theme 
and its usage please send an email to webteam@gatech.edu.

For more information on Georgia Tech website branding and 
visual identity guidelines please visit:
http://www.comm.gatech.edu/brand

VARIABLES

1. Colors

Buzz Gold #eeb211
Buzz Gold 60 #f5d376
GT Blue #00254c
GT Blue 80 #335170 (decorative)
Black 85 #262626
Dark Gray #545454
Medium Gray #808080 (decorative)
Light Gray #e5e5e5
GT Link Blue #004f9f (links only)
GT Link Hover #1879db (links only)


2. page.tpl.php

General utility variables:
- $base_path: The base URL path of the Drupal installation. At the very
  least, this will always default to /.
- $directory: The directory the template is located in, e.g. modules/system
  or themes/bartik.
- $is_front: TRUE if the current page is the front page.
- $logged_in: TRUE if the user is registered and signed in.
- $is_admin: TRUE if the user has permission to access administration pages.

Site identity:
- $front_page: The URL of the front page. Use this instead of $base_path,
  when linking to the front page. This includes the language domain or
  prefix.
- $site_name: The name of the site, empty when display has been disabled
  in theme settings.
- $site_slogan: The slogan of the site, empty when display has been disabled
  in theme settings.
- $gt_logo_file: The Georgia Tech logo image file as configured in theme settings.
- $site_title: The site title as configured in the theme settings.
- $site_title_class: The CSS class associated with the site title.
- $map_image: The map image for the super footer of the site as configured in theme
  settings.
- $street_address: The street address below the map in the super footer as configured
  in theme settings

Menus:
- $main_menu (array): An array containing the Main menu links for the
  site, if they have been configured.
- $secondary_menu (array): An array containing the Secondary menu links for
  the site, if they have been configured.
- $secondary_menu_heading: The title of the menu used by the secondary links.
- $breadcrumb: The breadcrumb trail for the current page.
- $social_media_links: Social media links menu from GT Tools module.
- $action_links: Action links menu from GT Tools module.
- $footer_links_1: Superfooter links menu 1 from GT Tools module.
- $footer_links_2: Superfooter links menu 2 from GT Tools module.
- $footer_links_3: Superfooter links menu 3 from GT Tools module.
- $footer_ulinks: Footer utility links menu from GT Tools module.

Regions:
- $page['left_nav'] = Left Side Menu (displays above all other content in Left Sidebar)
- $page['left'] = Left Sidebar
- $page['content_lead'] = Area ABOVE Main Content (IGNORES right region)
- $page['help'] = Site help content
- $page['main_top'] = Area ABOVE Main Content (RESPECTS right region)
- $page['content'] = Main Content
- $page['main_bottom'] = Area BELOW Main Content (RESPECTS right region)
- $page['right'] = Right Sidebar
- $page['content_close'] = Area BELOW Main Content (IGNORES right region)

OTHER
@see template_preprocess()
@see template_preprocess_page()
@see template_process()

3. node.tpl.php

Available variables:
- $title: the (sanitized) title of the node.
- $content: An array of node items. Use render($content) to print them all,
  or print a subset such as render($content['field_example']). Use
  hide($content['field_example']) to temporarily suppress the printing of a
  given element.
- $user_picture: The node author's picture from user-picture.tpl.php.
- $date: Formatted creation date. Preprocess functions can reformat it by
  calling format_date() with the desired parameters on the $created variable.
- $name: Themed username of node author output from theme_username().
- $node_url: Direct URL of the current node.
- $display_submitted: Whether submission information should be displayed.
- $submitted: Submission information created from $name and $date during
  template_preprocess_node().
- $classes: String of classes that can be used to style contextually through
  CSS. It can be manipulated through the variable $classes_array from
  preprocess functions. The default values can be one or more of the
  following:
  - node: The current template type; for example, "theming hook".
  - node-[type]: The current node type. For example, if the node is a
    "Blog entry" it would result in "node-blog". Note that the machine
    name will often be in a short form of the human readable label.
  - node-teaser: Nodes in teaser form.
  - node-preview: Nodes in preview mode.
  The following are controlled through the node publishing options.
  - node-promoted: Nodes promoted to the front page.
  - node-sticky: Nodes ordered above other non-sticky nodes in teaser
    listings.
  - node-unpublished: Unpublished nodes visible only to administrators.
- $title_prefix (array): An array containing additional output populated by
  modules, intended to be displayed in front of the main title tag that
  appears in the template.
- $title_suffix (array): An array containing additional output populated by
  modules, intended to be displayed after the main title tag that appears in
  the template.

Other variables:
- $node: Full node object. Contains data that may not be safe.
- $type: Node type; for example, story, page, blog, etc.
- $comment_count: Number of comments attached to the node.
- $uid: User ID of the node author.
- $created: Time the node was published formatted in Unix timestamp.
- $classes_array: Array of html class attribute values. It is flattened
  into a string within the variable $classes.
- $zebra: Outputs either "even" or "odd". Useful for zebra striping in
  teaser listings.
- $id: Position of the node. Increments each time it's output.

Node status variables:
- $view_mode: View mode; for example, "full", "teaser".
- $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
- $page: Flag for the full page state.
- $promote: Flag for front page promotion state.
- $sticky: Flags for sticky post setting.
- $status: Flag for published status.
- $comment: State of comment settings for the node.
- $readmore: Flags true if the teaser content of the node cannot hold the
  main body content.
- $is_front: Flags true when presented in the front page.
- $logged_in: Flags true when the current user is a logged-in member.
- $is_admin: Flags true when the current user is an administrator.

Field variables: for each field instance attached to the node a corresponding
variable is defined; for example, $node->body becomes $body. When needing to
access a field's raw values, developers/themers are strongly encouraged to
use these variables. Otherwise they will have to explicitly specify the
desired field language; for example, $node->body['en'], thus overriding any
language negotiation rule that was previously applied.

@see template_preprocess()
@see template_preprocess_node()
@see template_process()
@ingroup themeable

4. Block Class Styles:
For Vertical Menu blocks:
gt-menu-style-1 = MENU 1: Gold-Black, Gray-White
gt-menu-style-2 = MENU 2: Black-Gold, reversed
gt-menu-style-3 = MENU 3: Black-White, Gray-Black

To add Icons before the block Title:
icon-info-title = ICON: Information
icon-alert-title = ICON: Alert
icon-download-title = ICON: Download
icon-link-title = ICON: Link
icon-institution = ICON: Institution
icon-mortar-board = ICON: Mortar Board

To change the background and text color of the block Title:
block-title-bg-gt-gold = TITLE: Gold
block-title-bg-gt-blue = TITLE: Blue
block-title-bg-gray = TITLE: Gray
block-title-bg-black = TITLE: Black

Block Design Treatments:
promo-block = STYLE: Promotional (light gray body background, thick gold line at top and bottom)
related-info-block = STYLE: Related info (gray body background, gold title)

5. CKEditor styles:
Floats (mainly for use with images):
editor-float-left
editor-float-left-simple
editor-float-left-70
editor-float-left-50
editor-float-left-30
editor-float-right
editor-float-right-simple
editor-float-right-70
editor-float-right-50
editor-float-right-30
editor-clear-float-left
editor-clear-float-right
editor-clear-floats

Text alignment:
gt-ed-left-justify-text
gt-ed-center-justify-text
gt-ed-right-justify-text

Style treatments:
highlight-link-yellow
highlight-link-blue
highlight-link-gray
jump-link
intro-text
cutline-text
pull-quote-left
pull-quote-right
editor-icon-pdf
editor-icon-doc
editor-icon-xls
editor-icon-ppt
editor-icon-file
media_embed

Hidden headings:
h1-h6 class = element-invisible element-focusable

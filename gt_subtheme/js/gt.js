/**
* @file
* js base functions
*
* Includes js behaviors for various toggles and switches
*
*/

(function($) {
  Drupal.behaviors.gt = {
    attach: function (context, settings) {

      // remove colorbox functionality for screens less than 600px wide
      $(function() {
        if ($(window).width() < '600') {
          $('a.colorbox').removeClass('cboxElement');
        }
      });

      // Slide out menu
      $('#primary-menus-toggle, #primary-menus-close').click(function(){
        $('#primary-menus-off-canvas').toggleClass('reveal');
        return false;
      });


      // Site search toggle
      $('#site-search-container-switch').click(function(event){
        if ($('#site-search-container').hasClass('element-invisible')) {
          $('#site-search-container').removeClass('element-invisible');
        } else {
          $('#site-search-container').addClass('element-invisible');
        }
        if ($(this).parent('div').hasClass('search-local')) {
          $('#edit-keys').focus();
        }
        event.stopPropagation();
        event.preventDefault();
      });

      // adding hover-active class to parent main menu <li>
      $("#main-menu-wrapper > ul.menu > li").hover(
        function () {
          $(this).addClass('hover-active');
        },
        function () {
          $(this).removeClass('hover-active');
        }
      );

      // Main menu functionality
      var vernums = $.fn.jquery.split('.');
      if (parseInt(vernums[0]) > 0 && parseInt(vernums[1]) >= 7 && parseInt(vernums[2]) >= 0) {
        $("#main-menu-wrapper ul li a").on('touchstart click', function(event) { return handleTouchMenu(event, $(this)); });
      } else {
        $("#main-menu-wrapper ul li a").live('touchstart click', function(event) { return handleTouchMenu(event, $(this)); });
      }

      function handleTouchMenu(event, element) {
        // if toggle button for menu is visible then we're in a mobile viewport, so add accordion behaviors
        if($('#primary-menus-toggle').is(':visible')){
          if(element.hasClass('been-clicked') || !element.parent('li').children('ul').length > 0) {
            // fix for IOS requiring triple-click to navigate to expanded menu item
            window.location.href = element.attr('href');
            return true;
          } else {
            event.preventDefault();
            element.addClass('been-clicked');
            element.parent('li').children('ul').addClass('show');
            return false;
          }
          // else in desktop viewports
        } else {
          // if mega menus are in play
          if(element.hasClass('menu-minipanel')) {
            if(element.parent('li').hasClass('hover-active')) {
              return true;
            } else {
              // get index number of parent li
              var c = element.parent('li').index();
              // close all open mega menus
              $("#main-menu-wrapper ul li a").trigger("mouseout");
              // remove been clicked class from all other items
              $("#main-menu-wrapper ul li").each(function() {
                if (element.children('a').hasClass('been-clicked') && element.index() != c) {
                  element.children('a').removeClass('been-clicked');
                }
              });
              // go to URL if been-clicked class exists
              if (element.hasClass('been-clicked')) {
                element.trigger("mouseout");
                element.removeClass('been-clicked');
                return true;
              } else {
                // else trigger mouseover, add been-clicked class
                element.trigger("mouseover");
                element.addClass('been-clicked');
                return false;
              }
            }
          } else {
            // in standard drop-down only make second click necessary for top level items,
            // unless parent <li> has the "hover-active" class, which means we're not in a touch environment
            if(!element.hasClass('been-clicked') && element.parent('li').parent('ul').index() == 0) {
              if (element.parent('li').children('ul').length > 0) {
                element.addClass('been-clicked');
                // return true if hover class exists (cause we're not in touch environment)
                if (element.parent('li').hasClass('hover-active')) {
                  return true;
                } else {
                  return false;
                }
              } else {
                return true;
              }
            } else {
              return true;
            }
          }
        }
      }

      // Main menu functionality addendum for "nolink" parent <li> items
      // that are constructed via special menu items module
      $("#main-menu-wrapper > ul.menu > li > span.nolink").click(function() {
        // if toggle button for menu is visible then we're in a mobile viewport, so add accordion behaviors
        if($('#primary-menus-toggle').is(':visible')){
          if($(this).hasClass('been-clicked')) {
            $(this).removeClass('been-clicked');
            $(this).parent('li').children('ul').removeClass('show');
          } else {
            $(this).addClass('been-clicked');
            $(this).parent('li').children('ul').addClass('show');
          }
        }
      });

      // Superfooter main display toggle
      $('.js__superfooter-trigger').click(function(){
        var h = $('#superfooter > .row').outerHeight();
        if($(this).hasClass('collapsed')) {
          $(this).removeClass('collapsed');
          // TODO: figure out some easing that works in all viewports
          if ($(window).width() > '815') {
            $('#superfooter').animate({height: h + "px"}, 250, 'swing');
          } else {
            $('#superfooter').removeClass('collapsible');
          }
        } else {
          $(this).addClass('collapsed');
          // TODO: figure out some easing that works in all viewports
          if ($(window).width() > '815') {
            $('#superfooter').animate({height: "0px"}, 250, 'swing');
          } else {
            $('#superfooter').addClass('collapsible');
          }
        }
        return false;
      });

      // Superfooter links toggle
      $('.superfooter-resource-links .title').click(function(){
        if($(this).next().is('ul')){
          $(this).next().toggle();
          if ($(this).hasClass('open')){
            $(this).removeClass('open').addClass('closed');
          } else {
            $(this).removeClass('closed').addClass('open');
          }
          return false;
        }
        return true;
      });

      //code ends

    }
  };
})(jQuery);
